# 17 December 2020 MetaSat Meeting Minutes

**Time:** 15:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Pierros Papadeas, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; OSCW - Open Source CubeSat Workshop)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

<!--
**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates  

### LSTN Updates

* Heard back from library in Santiago, Chile
    * Have not heard from them because the library has been mostly shut down and librarians are not working from home
    * Hopeful we will be able to plan with them more in the spring as the vaccine rolls out
* Getting satellite community members involved with LSTN communities
    * Maybe volunteer translations? This is something that has happened for SatNOGS! Would be nice because translation is expensive
    * In some cases volunteer translations can be helpful, since the translators will presumably be experts in satellite stuff and may be able to translate technical material well
    * Will probably post about it on on Element/Riot a few times and see what kind of response we get

### MetaSat Updates

* Gearing up for MetaSat release
* Meeting with someone from Symbios who works on both the [CEOS database](http://database.eohandbook.com/) and [EOPortal](https://directory.eoportal.org/web/eoportal/satellite-missions)
    * CEOS wants to use MetaSat concepts to restructure their database
    * Shared [their metadata](https://drive.google.com/file/d/1xbP9UJgneYI38pRCEPNbex3gGRXHb1ib/view) with us
    * Will help us with the connection to EOPortal after the holidays
* Met with group at [Open Source Satellite](https://opensourcesatellite.org/), want to collaborate with MetaSat
    * Originally reached out to us after OSCW 2019

### LSF Updates

* Pierros had a meeting with UNOOSA (United Nations Office for Outer Space Affairs)
    * Talked about [access to space for all](https://www.unoosa.org/oosa/en/ourwork/access2space4all/index.html)
    * Talked with them about LSTN; could be more approachable, with fewer resources needed than some other programs
    * Will hopefully be able to chat about it more in January

### Wolbach Updates

* Heard back from ARDC&mdash;Still reviewing, will probably hear back in mid-January
