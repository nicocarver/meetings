# 10 September 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Pierros Papadeas, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates  

### LSTN Updates

* Marathon Library put up their [ground station](https://network.satnogs.org/stations/1730/)
  * Still testing, have had some networking problems
  * They may not be able to work on it for a few weeks, can we work on it remotely? If they give us access for example
  * A few options from SatNOGS: Set up a virtual network and set debug to send logs to SatNOGS 
* Working on online educational materials
  * Have first draft sent out for feedback, still waiting for a few libraries to get back
  * Once we hear back, will send to more people for broader feedback; hopefully next week
* First 3 education sessions are about satellite communication and communication modes; starting with center wave and Morse code, then move to Slow Scan TV and NOAA transmissions
  * Where should the educational materials go? On a webpage or something more modular?
  * How will they connect to the SatNOGS Wiki?
* Potential later sessions: How to schedule an observation, set up SatNOGS stuff yourself
* Going to try to get library stations up by spring

### MetaSat Updates

* Working on how we can cover TLE metadata
* Last minute crosswalking
  * Finished crosswalking [Jonathan McDowell's GCAT](https://www.planet4589.org/space/gcat/index.html), should be live soon
  * Also working on crosswalking some Wikidata-like vocabularies and [QUDT](http://www.qudt.org/)
* CubeSat reference model group: Still having meetings, getting glossary of terms from Dave Kaslow soon
* After this, will work on documentation and gathering feedback

### LSF Updates

* Made some accessibility fixes for [SatNOGS DB](https://db.satnogs.org/) site
* Starting to work on Satellite ID issues
  * Will affect all parts of SatNOGS (DB, network, client...) as well as MetaSat (necessary metadata elements)

### Wolbach Updates

* Working on funding for next stages of MetaSat and LSTN; should be meeting with Sloan contacts about framing for MetaSat proposal soon
