# 8 October 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |
<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates  

### LSTN Updates

* LSTN kit as a reference kit?
  * Shared with the SatNOGS community as a kit they can buy and put together as something they know will work
    * Put on the SatNOGS wiki?
  * Is the LSTN kit performing as expected, are there any changes we could make to improve it?
    * Right now, seems to be working fine? Pierros may have further insight, but from observations we have seen, seems fine.
* Back in touch with Marathon, TX library
* Sent forms asking for feedback to a couple of people who have contacted LSTN because they are interested in public space education
  * We want to get some ideas from people outside of our current immediate orbit

### MetaSat Updates

* Working on updating some text for the site, right now on the landing pages for crosswalks and different iterations of elements (families and segments, for example)
* Starting work on JSON-LD generator - deciding what scope it will have
  * Ideas: Drag and drop elements, edit existing files

### LSF Updates

* Still working on client
* Working on new waterfall artifact, getting closer!
* Working on satellite ID, will help move forward with more development where it is needed

<!--
### Wolbach Updates

* x
-->