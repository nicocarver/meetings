# 18 March 2021 MetaSat Meeting Minutes

**Time:** 15:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Nikoletta Triantafyllopoulou, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->
## Updates  


### LSTN Updates

* LSTN outreach updates
  * Worked on outline for first blog post; will be able to share that tomorrow on Google docs
  * Researching online places (groups or platforms) where we can crosspost; will also have an update tomorrow
    * Reddit and Quora spaces that might work
* Santiago has had a successful observation, but having technical difficulties now
  * Might be an SDR issue; this has been an issue from the beginning, might do RTL SDR instead of Lime SDR in future kits
  * LSF might be in charge of putting together the kits in the future, so then Wolbach *et al.* can purchase directly from LSF in the future instead of chasing around components
  * Any other problematic components that we know of? Might want to upgrade antenna from UHF to VHF
  * Let's come up with short and long term plans to improve the kits over time
* Weblate: Looked into existing resources for translators; haven't found a lot
  * Weblate GitHub has a [discussion board](https://github.com/WeblateOrg/weblate/discussions) that might be a good place to talk about this with the community
  * Will add a "resources for new translators" thread on discussions board
    * Might help us find resources others have already made!

### MetaSat Updates 

* Connecting with NASA's S3VI (Small Spacecraft Systems Virtual Institute); people seem interested in MetaSat!
  * [Webinar](https://www.nasa.gov/smallsat-institute/wikipedia-for-smallsats-the-ssri-knowledge-base) Daniel went to
* Finished MINXSS example file, will update on [GitLab](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/examples/minxss_example.jsonld) soon

### LSF Updates

* API deployment; going well but a little behind schedule

### Wolbach Updates

* Daina met with Pierros and some others to work on fleshing out plans for NASA ROSES proposal
  * We have figured out role LSF can play, since NASA will not directly give money out of the country
    * Basically would have Harvard directly hire open-source developers for the library
