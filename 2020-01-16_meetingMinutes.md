# January 16, 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Fredy Damkalis, Manthos Papamatthaiou  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #10 | Contact potential partner libraries | Planning kickoffs with current partners | NC |
| #34 | Draft LSTN handbook | Hopefully finishing up soon | NC |
| #36 | Research radio reception laws in Moldova | No update | PP |

**All other actions were completed or are no longer relevant.**

<!--
### Logistics  
* x


## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #38 | ---------------- | --- |
-->

## Updates

### LSTN Updates

* Working on kickoff meetings with TX and Moldova partner libraries
* Still looking for last southern hemisphere library; might move to equator
* Hoping for handbook to be finished up soon
* Tested Cambridge Public Library rooftop for EM interference; all looked good! Ready to go for April launch.
* Wolbach build should happen soon; waiting for final approval. Planning on building and testing this month.
* LSTN logo--making progress! Want a sticker to put on the side of the ground stations
  * Will say: "A project from the CfA, powered by SatNOGS"

### MetaSat Updates

* Daniel will be posting a lot in the [Riot channel](https://riot.im/app/#/room/#metasat:matrix.org)
* Lots of feedback from Javier (made them into MetaSat issues)
* Please look through the [MetaSat issues](https://gitlab.com/metasat/metasat-schema/issues)
  * Four important ones: [17](https://gitlab.com/metasat/metasat-schema/issues/17) (frequency ranges), [24](https://gitlab.com/metasat/metasat-schema/issues/24), [25](https://gitlab.com/metasat/metasat-schema/issues/25) (both about link parameters), [27](https://gitlab.com/metasat/metasat-schema/issues/27) (URLs for each MetaSat element)
* Potential 1.0 release date: Mid-February (15th-ish)

### LSF Updates

* Working on network right now (and DB) to be ready for any changes needed to implement the schema

### Wolbach Updates

* Planning a bigger meeting on January 30th to talk about HDF5
  * To see how we can integrate artifacts into MetaSat
