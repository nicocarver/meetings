# 27 May 2021 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Nikoletta Triantafyllopoulou, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->
## Updates  


### LSTN Updates

* Still troubleshooting stations in Santiago and at Wolbach
  * Next step with Santiago: Switching to RTL-SDR; have to wait for them to receive it
  * Current issue: Observations not ending when scheduled
    * LSF knows about this issue, should be fixed with next version of the client.
* Cambridge Public Library: Has had a networking issue, but they think they have figured it out.
* [Blog post](https://libre.space/2021/05/26/help-us-translate-the-lstn-handbook/) is up!
  * Has also been shared on LSF's social media profiles
  * Wolbach wants to share on their blog, but should not post an exact copy - will affect Google statistics
    * Can be rewritten, or can make a summary and link to LSF blog post
  * Might ask partner libraries to share the blog post on their socials, too

### MetaSat Updates 

* Working with [SSRI knowledge base](https://www.nasa.gov/smallsat-institute/ssri-knowledge-base)
  * [Another SSRI link](https://s3vi.ndc.nasa.gov/ssri-kb/)
* Also [brainstorming](https://community.libre.space/t/wikidata-and-satnogs-linked-data/7961/3) with LSF user on assigning Wikidata items to smallsats using [Mix-n-Match](https://mix-n-match.toolforge.org/#/)
  * Will help make sure MetaSat is integrated with Wikidata technologies and more semantic web stuff
* Might be nice to write up a MetaSat explainer
  * Nikoletta could start on this in a few weeks
  * Might also be a good thing for Daniel to work on when the MetaSat paper is done

### LSF Updates

* Would LSF be able to make a client that would update automatically? Is this on your list of goals, whether or not we get NASA funding?
  * Would be easier for partner libraries
  * Hasn't been discussed in LSF before, but they will think about it
* Deployed changes in the API
  * Will be testing them in dev environment next week

<!--### Wolbach Updates

* x-->
