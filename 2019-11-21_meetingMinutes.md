# November 21, 2019 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Pierros Papadeas, Nico Carver, Daniel Chivvis, Allie Williams, Vasilis Tsiligiannis, Fredy Damakalis   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; OSCW - Open Source CubeSat Workshop)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #10 | Contact potential partner libraries | Namibia looks good; updates on others below | NC |
| #22 | Email specific individuals for feedback | Shared spreadsheet of people from conferences (COSPAR and OSCW) and made issue confidential | DC |
| #34 | Draft LSTN handbook | No update | NC |
| #35 | Make decisions on LSTN laptop | No update | DB, NC |

**All other actions were completed or are no longer relevant.**

<!--
#### Logistics  
* Zabra speakers work well for group meetings fyi
-->

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| [MetaSat Schema 4](https://gitlab.com/metasat/metasat-schema/issues/4) | LSF: give feedback to Daniel on new schema | -- |

## Updates

### LSTN Updates

* Namibia: Finalizing talks, looks promising so far!
* South America: Moving on from Brazil, will look at Ecuador, Uruguay, and Moldova next.
* Rural US: Moving on from Fort Peck, will try Salish Kootenai College library (D'Arcy McNickle Library) next

### MetaSat Updates

* When can we start integrating schema with SatNOGS DB? 
  * At least a timeline to start with a dev instance?
  * Timing depends on completeness of schema with fields important for SatNOGS. Will discuss more later.
* Daniel rundown on schema changes
  * Making crosswalk based on [Codemeta crosswalk](https://codemeta.github.io/crosswalk/)
    * Our draft crosswalk is [here](https://docs.google.com/spreadsheets/d/1aBs6vy0-6ArELwqVZhJ1OOPFp-UVZOesaR118OYjoG4/edit?usp=sharing)
      * Wikidata properties are only things that already exist--haven't made new ones yet, but might later
      * When mapping to Wikidata, also map to other places that are stored in Wikidata!
    * Also a list of [standards](https://docs.google.com/spreadsheets/d/1lgyRqDJkC2Qr0Bra7VjRoqjz-sjKILV_iZ8W1ydIqsA/edit?usp=sharing) we might want to crosswalk too
    * All of these documents have been added to the [Wiki](https://gitlab.com/metasat/meetings/wikis/home), as well
  * Haven't made many revisions of the actual schema yet--will be integrating feedback from COSPAR soon
    * Expect a large schema update within the next week (by November 28)
      * Will also include an update in Riot
    * Want feedback from LSF in early December

<!--
### LSF Updates

* x -->

### Wolbach Updates

* Science fair at Cambridge Public Library, April 17-26 2020
  * Speaker from LSF to talk at CPL? 
    * April 17-20 is a holiday, other days are possible
* NASA and MIT still excited to collaborate on educational materials. Any specific activities we could bundle into proposal for MetaSat/LSTN?
  * Feedbook on instructions in LSTN handbook to make sure they are accessible to everyone
