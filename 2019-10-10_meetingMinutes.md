# October 10, 2019 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:**  Daina Bouquin, Katie Frey, Pierros Papadeas, Nico Carver, Daniel Chivvis, Allie Williams, Fredy Damakalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; OSCW - Open Source CubeSat Workshop; SSA - Space Situational Awareness)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | --- | --- | --- |
| #10 | Contact potential partner libraries | Will take a more proactive approach, more updates in LSTN section | NC |
| #21 | Draft list of stakeholders to contact | After OSCW | DC, AW |
| #22 | Email specific individuals who may want to give feedback on MetaSat schema | After OSCW | DC |
| #30 | Draft emails to LSF contacts; run by LSF | After OSCW | DC |
| #31 | Confirm workflow for changes in schema and sharing changes with Libre Space | After OSCW | DB |
| #33 | Draft LSTN document: What libraries can do with a ground station | Ongoing | NC |

**All other actions were completed or are no longer relevant.**

<!--
## Action Items

| Issue number | Summary | Assignee |
| --- | --- | --- |
| #34 |  |  |
-->

## Updates

<!--
### Logistics  
* Conference phone group mic works well
-->

### OSCW - Open Source CubeSat Workshop

* Daniel and Katie will be in Athens for OSCW next week!
* Want to meet up to talk about:
  * Timeline for installing at Wolbach
  * Walkthrough of figuring out identification issues
    * Metadata for SSA - how can we integrate this into the schema?

### LSTN Updates

* Will try to be more active when contacting potential partner libraries
  * We don't want the libraries to think we are sending out mass emails!
* Found library to contact in Namibia: [Winkhoek Public Library](https://docs.google.com/document/d/1YuTjw8Nsmjo7C96hUdUPZ9xaxaEJ5PjJsw8TwaLg3co/edit)

<!--
### MetaSat Updates

 * x
-->

### LSF Updates

* Vasilis and Fredy working on software side
* Next mission: building two [PocketQubes](http://www.albaorbital.com/)
  * More information [here](https://libre.space/projects/pocketqubes/), and the GitLab repository is [here](https://gitlab.com/librespacefoundation/pq9ish)
  * Launching this winter
  * Goals:
    * Try to pick up new types of artifacts
    * Try to distinguish between the two
      * Common problem: How do we know which sat we're talking to? (space situational awareness, SSA)
      * Will want to incorporate any solutions into the schema
      * This can also be helpful for tracking debris, etc

### Wolbach Updates

* Recently met with Sloan contact - Josh Greenburg - he seems pleased with what we're doing, seems excited
* Oglala Lakota College, which [Woksape Tipi](https://docs.google.com/document/d/1Q2EEmpsHHMWShO1tuawcSvSRYsbuRxE-6Kpl6DjeDAg/edit) serves, has Sloan connections, too
  * Could positively effect funding for educational resources

<!-- Links -->
