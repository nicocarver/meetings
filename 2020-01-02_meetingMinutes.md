# January 2, 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Pierros Papadeas, Nico Carver, Daniel Chivvis, Allie Williams, Vasilis Tsiligiannis, Fredy Damkalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; AAS - American Astronomical Society, which has an annual meeting coming up in January; HDF5 - Hierarchical Data Format 5, a file format used to both store and organize data)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #10 | Contact potential partner libraries | Marathon TX is our confirmed 3rd partner! Still waiting on a fourth (Southern hemisphere) | NC |
| #22 | Email specific individuals for feedback | Keep open until AAS | DC |
| #34 | Draft LSTN handbook | Ongoing | NC |
| #36 | Research radio reception laws in Moldova | Nothing solid yet, will need to reach out to local radiometers | PP |

**All other issues have been closed.**

<!--
### Logistics  
* x


## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #38 | ---------------- | --- |
-->

## Updates

### LSTN Updates

* Marathon, TX: [Marathon Public Library](http://www.marathonpubliclibrary.org/)  
  * Our rural US partner confirmed!
  * More information about Marathon and the library [here](https://docs.google.com/document/d/16-YrzmjZ6_smmvwF3OU7DshNRnoSmOdlzCXixdR13uQ/edit).
  * Town of 400 with donation-supported community library. One staff member.
  * Apparently a lot of astronomers visit because it is one of the darkest places in the US
* Joining [Cambridge Public Library](https://www.cambridgema.gov/cpl.aspx) and [Biblioteca Municipal&abreve;](http://www.hasdeu.md/) as our confirmed partner libraries
* Namibia is slow-moving. Looking at Antarctica and Mauritius
  * Possibly [Mahatma Gandhi Institute](https://www.mgirti.ac.mu/) in Mauritius&mdash;nonprofit, open to public but 18+
* Working on [LSTN site](https://lstn.wolba.ch) still. Added links to SatNOGS. Can we have feedback before AAS?
* Moldova: Is it ok to put a receiving station here? What are the laws like?
  * Couldn't find a specific legislation that did not permit a station...but that doesn't mean it's permitted.
  * Best bet: Reach out to local radiometer club (there is one in the capital)
  * Is there a SatNOGS station in Moldova?
    * Former station from Technical University of Moldova. Can we reach out to the owner(s)?

### MetaSat Updates

* GitLab repo has been rearranged a bit
  * 1: Just one Metasat.jsonld file
  * 2: Context file
  * 3: Folder for examples
  * Thank you to LSF for the feedback, it's perfect!
* AAS: Will have a MetaSat splinter meeting to get lots of feedback
  * Then, when we get back we will release v1.0!
* [README](https://gitlab.com/metasat/metasat-schema/blob/master/README.md) - has information about the 4 JSON-LD algorithms&mdash;Expansion, compaction, flattening and framing. 
  * All four algorithms can be very helpful for implementing JSON-LD with your database
  * Hopefully the explanations will help with people who are are unfamiliar with JSON-LD

### LSF Updates

* HDF5 Stuff
  * Should we set up seperate calls with Vasilis/Fredy to talk about this? To help incorporate it into the schema
* Timing&mdash;when will the database be able to move over to LD? Have some limitations.
  * But should mostly be able to use it directly.

### Wolbach Updates

* Spring/April talk at Cambridge Public Library&mdash;who from LSF is coming? We're assuming 2, but could maybe bring 3.
  * How long? Maybe 2 days, 3 nights
  * Two people is fine, but will talk about it later.
  * Potential giveaway?
* Branding: MetaSat logo is ready, LSTN is in progress
  * How should we use it? What kind of swag should we get?
  * Get a sticker to go on the stations! With LSTN and SatNOGS logos
