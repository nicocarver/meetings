# 27 August 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Pierros Papadeas, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates  

### LSTN Updates

* Communicating with librarian in Marathon; their station is up, but not online (network configuration issues)
* Working on education activity with Morse code - video introduction
  * Next, will send to pilot librarians for feedback, and solicit ideas for more activities

### MetaSat Updates

* New list of elements, updates, and deletions; should be updated tomorrow
* Looking more into RDF, putting together a file of MetaSat elements; hopefully by next week
* Meeting with Dave Kaslow about [CubeSat reference model](https://www.incose.org/incose-member-resources/working-groups/Application/space-system) (data model for CubeSats)

### LSF Updates

* Will open GitLab issue for SatNOGS DB UI accessibility issues
* Still working on satellite ID, will be testing soon
* Working on a new vetting system relating to artifacts
* Vasilis working on waterfall artifact

<!--
### Wolbach Updates

* x
-->