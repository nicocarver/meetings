# 9 April 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Fredy Damkalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update | PP |
| #40 | Feedback on spreadsheets for MetaSat terms | Ongoing | LSF |

**All other actions were completed or are no longer relevant.**

<!--
### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #41 |  | XX |
-->

## Updates

### LSTN Updates

* Thank you for the feedback on build instructions! 
* Now, writing "About" section that will introduce people involved in LSTN
  * Is there a group photo of SatNOGS LSTN contributors that we can use?

### MetaSat Updates

* Working on Attitude Control system
  * Should have list of elements done by end of the week
* Working on [JSON-LD file for electrical power system elements](https://gitlab.com/metasat/metasat-schema/-/blob/master/Examples/ElectricalPowerSystems.jsonld), as well
* Editing some existing elements in the [MetaSat DB](https://schema.space/metasat/) - including descriptions
* Input from Manolis - transmission schema
  * On GitLab [here](https://gitlab.com/librespacefoundation/satnogs/satnogs-db/-/issues/317)
  * These are all terms/parameters that are already in use by SatNOGS, so we want to make sure to include them in the schema
  * SatNOGS uses these parameters to help make better decoders and demodulators for users
* Radiometer modes and transmissions being added

### LSF Updates

* Progress to start implementing MetaSat?
  * Maybe want to start thinking about user interfaces
    * [CodeMeta generator](https://github.com/codemeta/codemeta-generator)
    * A similar tool could make it easier for people to make their own files to ingest into SatNogs
  * Right now SatNOGS is working on architectural changes
  * More focus is on the client right now, but hopefully can start working towards MetaSat implementation next week

<!--
### Wolbach Updates

* x
-->
