# 02 July 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |
| #41 | Add MetaSat primer to schema repository | No update | AW |
| #42 | Make plans for updating website | Will work on plans for "Resources" section this week | AW |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates

### LSTN Updates

* Ground station package for library in Chișinău, Moldova is still stuck in customs
  * Looking for solution to pay for customs fee
* We also have a package heading to Santiago, Chile - working on paperwork and might try to contact FedEx in Santiago to mitigate similar issues
* How do we scale up with international shipping issues?
  * Might want a position dedicated to this task in the future

### MetaSat Updates

* Working on the [crosswalk](https://schema.space/crosswalk/) still - it's on the website!
  * Consider this version 1 - sources will be added/removed
* Working on adding and removing elements, too
* Revitalized sorting tool
  * Rough copy of space segment on the [sorting tool](https://metasat.schema.space/) now
  * Not in website navigation yet, will probably be put in a new "resources" page soon
  * Maybe add documentation/explainer on sorting tool page (to explain that it isn't a perfect match with our examples, etc.)
* Adding accessibility to the website
  * Added changes to make it easier to navigate the website with a keyboard - like a hidden "Skip to main content" link in the navbar
  * Eventually want to make sure the site works with a screen reader - right now planning on using [NVDA](https://www.nvaccess.org/), might also try [JAWS](https://support.freedomscientific.com/Downloads/JAWS/JAWSWhatsNew#Enhancements)
* Next step for website: Adding a "Resources" page

### LSF Updates

* Still working on integrating JSON-LD with SatNOGS APIs. Right now, three different APIs can be used with JSON-LD:
  * [Satellites](https://db-dev.satnogs.org/api/satellites/)
  * [Transmitters](https://db-dev.satnogs.org/api/transmitters/)
  * [Modes](https://db-dev.satnogs.org/api/modes/)
  * On any of these pages, click "GET" in top right, then "JSON-LD"
  * There is one more, but it is behind a key; will share in Riot
* Should we give users expanded (machine-readable) or compacted (human-readable) version of JSON-LD?
  * Can give either, they are the same information and can be converted into each other with no information loss
  * Maybe give the possibility to download either; while algorithms exist, users may or may not be able to use them.
* Posting JSON-LD in pages of website - for web crawlers, etc.
* Vasilis is working on the client still

### Wolbach Updates

* SmallSat conference - poster due date has been moved up! Have to have ours in by 8 July
