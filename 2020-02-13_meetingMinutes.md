# 13 February 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Pierros Papadeas, Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Fredy Damkalis   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #10 | Contact potential partner libraries | Updates below | NC |
| #34 | Draft LSTN handbook | --- | NC |
| #36 | Research radio reception laws in Moldova | --- | PP |
<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x


## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #38 | ---------------- | --- |
-->
## Updates

### LSTN Updates

* Waiting for final word from Mahatma Gandhi Institute in Mauritius
  * Not sure how long we will wait before moving on to other potential partners
* Meeting with MIT tomorrow about education ideas
  * Narrowing down ideas to figure out what will work best in public libraries

### MetaSat Updates

* Ground Segment v1.0 is conceptually done
  * Check on the Table View&mdash;it's a [PDF](https://drive.google.com/a/cfa.harvard.edu/file/d/1-_zN9W-liH3214M0r5xqg7TMQSieEpso/view?usp=sharing) now because the file got too big
    * Unfortunately can't see comments in this form
  * Will start moving these terms into our JSON-LD file this week
  * We will make issues for this group of terms and welcome feedback there
  * Especially asking for feedback on: Cables, connectors, power terms
    * E.g., different sources describe power differently, so want to figure out a best practice
    * Cables and connectors might be pretty granular
    * We received a suggestion that we should add meteorological instruments on the ground station
      * At this stage that might be a little much
* Will edit/make a QUBIK JSON-LD example file and then revamp Space Segment, to get an idea of what we should include
  * Since QUBIK is a small PocketQube, it will be missing some things that other missions will have
  * Another example to look at: [OreSat](http://oresat.org/) (Oregon University)
* Next step: minting URLs for each term
  * Potential directory landing page [here](https://schema.space/metasat)
  * Example term page [here](https://schema.space/metasat/observation)
  * Right now it's a WordPress site, we might migrate to something else because we'll need several hundred


### LSF Updates

* New SatNOGS client release coming soon, hopefully by the end of this week
  * [Station 977](https://network.satnogs.org/stations/977/) - testing the new client
* Will work with RBPi 3 or 4 and multiple versions of Buster, so ground stations with older setups will still be able to use it.
* There are three important updates to the client:
  * Support of multiple SDRs
  * Ability to restructure code so it works well with MetaSat
  * Changes in demodulation so ground station can pick up more signals

<!--
### Wolbach Updates

* x
-->
