# 04 June 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Fredy Damkalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x
-->

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #41 | Add MetaSat primer to schema repository | AW |

## Updates

### LSTN Updates

* Still in limbo getting ground station kit to Moldova

### MetaSat Updates

* Daniel is working on paper and crosswalk. Also will potentially add and remove some elements pending Fredy's feedback.
* [MetaSat explainer draft](https://docs.google.com/document/d/1PILm-QZg63PMkYWuhuHgaUBSUDf_xeXXmwN4E3zxYzA/edit) and [RDF primer](https://docs.google.com/document/d/1WtcYgWDOnLCJ4EVuP3BmYFzGkBVYsG9aq8lv5c7nojI/edit)
  * Should this go on the GitLab? In README or another place? Link from README to extra file, for example.
    * README is an "about" page; add other stuff into other markdown pages
    * Once the version on the Google doc is finished, will convert to markdown and add to both repo and website.

<!--
### LSF Updates

* x
-->

### Wolbach Updates

* Followup with Sloan Foundation soon to talk about a potential future funding timeline
