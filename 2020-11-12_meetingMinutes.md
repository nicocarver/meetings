# 12 November 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

<!--
**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->
## Updates  

### LSTN Updates

* Proposal for ARDC is in the pipeline! Hoping to hear from them soon
* Nico working on introductory materials
  * Using [reveal.js](https://revealjs.com/), an open-source HTML presentation format (should be more stable than some other technologies)
* Issues with Marathon station - might be a hardware issue with their Internet
  * Getting a new router soon

### MetaSat Updates

* [Wikidata Property](https://www.wikidata.org/wiki/Wikidata:Property_proposal/Generic#MetaSat_ID) for MetaSat ID has been approved! 
  * Goes with the [Wikidata Item](https://www.wikidata.org/wiki/Q101095843) for MetaSat
  * Next step: Working with Wolbach student workers to get concepts up on Wikidata
* Still in contact with [PMPedia](https://pmpedia.space/)
* Working on MetaSat paper
* Crosswalk descriptions are available on the website

### LSF Updates

* Deployment this week, and issues with space track (TLE generation)
* More development on Satellite ID and some backend issues for moving forward with artifacts

<!--
### Wolbach Updates

* x
-->