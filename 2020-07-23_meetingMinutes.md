# 23 July 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Fredy Damkalis, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |
| #42 | Make plans for updating website | In progress; expect big update next week | AW |


<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates

### MetaSat Updates

* Working on website - planning big update next week - both front end and back end
* How do we version the schema?
  * Planning for this soon - drafting documents for versioning and release cycle
    * Hope to share next week
    * Want it to be similar to UAT
        * [UAT versioning document](http://astrothesaurus.org/about/versioning/)
        * [UAT release cycle document](http://astrothesaurus.org/about/release-cycle/)
  * Want template RDF files for different element use cases; both existing and deprecated
    * Eventually, want database to be able to make these automatically

### LSF Updates

* Demo site up for new SatNOGS DB UI changes
  * Leave feedback on Riot
  * New links for "contact us to be an editor" - want it to be crowdsourced
* Still working on artifacts behind the scenes too
* Also plan to archive SatNOGS DB; brainstorming possible solutions

### Wolbach Updates

* Brainstorming LSTN funding
  * [YASME](https://www.yasme.org/grants/)
  * [AMPR](https://www.ampr.org/giving/)