# January 23, 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Fredy Damkalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #10 | Contact potential partner libraries | Still in talks with Mauritius; kickoff meetings with the rest coming soon | NC |
| #34 | Draft LSTN handbook | Currently putting together build instructions, should be ready for feedback soon. | NC |
| #36 | Research radio reception laws in Moldova | Looks like we won't need a license, but want us to share the specs to double check. | PP |

<!--**All other actions were completed or are no longer relevant.**-->

### Logistics  

* Next week: Potential longer meeting about HDF5 and artifacts after regular check-in meeting
  * Should confirm by end of day tomorrow.

<!--
## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #38 | ---------------- | --- |
-->

## Updates

### LSTN Updates

* Possible partner (Southern hemisphere): Mauritius
  * Found out we probably won't need a license for ground station receiver
  * Want us to forward them specifications of ground station to make sure
    * Is there a specific set of specifications SatNOGS already shares with groups? 
    * Not right now, just the [wiki page](https://wiki.satnogs.org/Main_Page) with the setup.
    * What should we share with them?
    * Probably want to check to make sure it's only a receiving station; the radio part
* We have a logo!
  * [Logo to put on ground station](https://drive.google.com/a/cfa.harvard.edu/file/d/1ncOnJSsNWCL8XQXF19xupAILn9o4XEZ1/view?usp=sharing) says "Powered by SatNOGS"
* Kickoff meetings with partner libraries
  * Cambridge Public Library is done
  * Marathon and Moldova are next
* Educational plan
  * What potential questions should we be covering?
  * Probably want to chat about this more in-depth later
* Won't be able to send kits through embassy to Moldova
  * Can't send lithium batteries
  * Will FedEx directly instead

### MetaSat Updates

* MetaSat schema-specific issues 
  * [Issue 27](https://gitlab.com/metasat/metasat-schema/issues/27)&mdash;Daniel answered Fredy's questions
  * Talking with Javier about ground station elements
  * Early next week, should have: New set of issues, changes to ground station segment, new elements
  * Some changes exist in table view, but not JSON-LD or crosswalk
* Still on track for February 14 launch date for v1.0
  * Launch will include a ready-to-use LD file, directory of elements and element descriptions, and crosswalk on website.
* LSF will review current schema and issues soon

### LSF Updates

* Still working on updating Network and DB to implement schema more easily.

<!--### Wolbach Updates

* x-->
