# 1 July 2021 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Nico Carver, Daniel Chivvis, Katie Frey, Nikoletta Triantafyllopoulou, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->
## Updates  


### LSTN Updates

* Still troubleshooting Chilean ground station
* How can we make it easier for people to contact Wolbach people about LSTN stuff?
    * Will probably start with making a group LSTN email that forward to a few people involved
        * LSTN@cfa is already in use, so should be something else. Maybe at the schema.space domain?
        * Let Nikoletta know when it is ready and she will update the blog and other social media posts
* Nikoletta is still posting about LSTN translations, recently has crossposted to Reddit, for example
    * Will forward links to the subreddits posted to!
    * In general, it has had a very positive reception! 
    * Has been fully translated into Italian, too.

<!--### MetaSat Updates 

* x

### LSF Updates

* x
-->
### Wolbach Updates

* Still working on SAA with NASA - finally got a copy for our review, and sent it to Harvard/Smithsonian superiors for their feedback
