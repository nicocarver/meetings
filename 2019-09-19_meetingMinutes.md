# September 19, 2019 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Katie Frey, Pierros Papadeas, Nico Carver, Daniel Chivvis, Allie Williams, Fredy Damakalis<!-- anyone else? -->  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; JSON LD - JSON (a data-sharing file format) for Linked Data)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | --- | --- | --- |
| #7 | Attempt to obtain radio license in Namibia/Zambia | No response yet. Pierros will call them this week as a final attempt. | PP |
| #10 | Contact potential partner libraries | No updates. Follow up sent to Woksape Tipi, have emailed Indian and second Brazilian library | NC |
| #19 | Test LSTN kit | Operating as [ground station 977](https://network.satnogs.org/stations/977/); original BOM works, currently testing to see if it works with cheaper components | PP |
| #21 | Draft list of stakeholders to contact | Have contacted Dr. Michael Swartwout at SLU, runs a CubeSat [database](https://sites.google.com/a/slu.edu/swartwout/home/cubesat-database), will possibly act as collaborator; how does he gather his data? | DC, AW |
| #22 | Email specific individuals who may want to give feedback on MetaSat schema | Not yet; to do in a few weeks | DC |
| #30 | Draft emails to LSF contacts; run by LSF | Add to list Bryan Klofas at Planet (formerly CalPoly), also has a [CubeSat database](https://www.klofas.com/comm-table/) | DC |
| #31 | Confirm workflow for changes in schema and sharing changes with Libre Space | Emailed; suggestion from Fredy that we will incorporate | DB |
| #33 | Draft LSTN document: What libraries can do with a ground station | Still beginning, will have a draft next week for comments | NC |

**All other actions were completed or are no longer relevant.**

<!--
#### Logistics  
*

## Action Items

| Issue number | Summary | Assignee |
| --- | --- | --- |
| #34 |  |  |
-->

## Updates

### LSTN Updates

* Cambridge Public Library LSTN kit questions: Can they place their antenna on top of elevator housing?
  * Vibration shouldn't be an issue, but may be electromagnetic interference (in general, would want to avoid being near motors)
  * Could do a check by attempting to use a radio in that area to see if there is interference.
* Power over ethernet--how do we check that their ethernet network is ready for the kit?
  * Since it's outside, use UV-rated cables; in general equipment should be able to withstand being outside
  * No more than 100m point-to-point
  * Also, needs access to web outside, remote management setup
* Temperature range for kit? Will it survive the winter?
  * This will be a good test for that! It does hold up to high temperatures and humidity
  * SatNOGS does have ground stations in cold/snowy areas that are fine, but this setup is different
  * Since the kit produces heat, would probably have to be extremely cold for the temperature to be an issue

### MetaSat Updates

* Daniel is working through the JSON LD
  * Can we use this for the sorting tool?
  * Maybe not, the two are structurally different
  * Hard to discuss until we have the JSON LD to look at
* How does the Sorting Tool send feedback?
  * Emails a text file (diff file)
  * Uses + and - to show what was moved, added, deleted
  * The following example shows these changes:
    * "Mission Funder" and "Mission Funder Contact Info" are deleted (moved to "orphans")
    * A new concept called "Funding Source" is added as a child of "Mission"
    * "Mission Funder Supplemental Info" is moved from a child of "Mission People" to a child of "Funding Source"
    * The rest of the elements are added as children of "Funding Source"

-Mission People NT Mission Funder  
-Mission Funder NT Mission Funder Contact Info  
-Mission Funder NT Mission Funder Supplemental Info  
'+root  
'+root NT orphans  
'+root NT Mission  
'+orphans  
'+orphans NT Mission Funder Contact Info  
'+orphans NT Mission Funder  
'+Mission NT Funding Source  
'+Funding Source  
'+Funding Source NT Mission Funder Supplemental Info  
'+Funding Source NT Grant Number  
'+Funding Source NT Funding Program  
'+Funding Source NT Program Officer  
'+Grant Number  
'+Funding Program  
'+Program Officer  
