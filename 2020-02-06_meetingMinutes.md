# February 06, 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Fredy Damkalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; URI - Uniform Resource Indicator, [similar to a URL](https://stackoverflow.com/a/28865728); HDF5 - a data storage file format)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #10 | Contact potential partner libraries | Kickoff meetings with Moldova and Marathon, TX; checking out final details with Mauritius | NC |
| #34 | Draft LSTN handbook | No update | NC |
| #36 | Research radio reception laws in Moldova | No update | PP |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x


## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #38 | ---------------- | --- |
-->

## Updates

### LSTN Updates

* Kick off meetings with [Moldova](http://www.hasdeu.md/) and [Marathon, Texas](http://www.marathonpubliclibrary.org/) are done, went well, answering questions and talking logistics
* Still trying to nail down [Mauritius](https://www.mgirti.ac.mu/)
  * They're checking with their local authorities, we'll follow up in the next week

### MetaSat Updates

* Daniel and Daina restructured timeline
  * Moving our v1.0 release back to April
    * See details on the new timeline in the table below
  * Expect notes and issues on [schema repository](https://gitlab.com/metasat/metasat-schema) soon

#### Updated 2020 timeline  
| Date | Task |
| --- | --- |
| 14 February | Finish ground segment |
| 21 February | URIs and JSON-LD update for ground segment |
| 10 March | Finish a JSON-LD file to describe [QUBIK](https://libre.space/projects/qubik/) |
| 20 March | Update space segment |
| 31 March | Update observation and mission sections, including URIs |
| 30 April | Example artifacts file with HDF5 metadata |
| | Element descriptions for each URI page |
| | Updated sorting tool
| Summer 2020 | Compresensive MetaSat documentation and a paper


### LSF Updates

* Working on new client release and [QUBIK](https://libre.space/projects/qubik/) 
* [Cambridge Science Festival](https://www.cambridgesciencefestival.org/) this spring&mdash;any details?
  * We know that it is between the 17th and 26th of April, but haven't had any updates about when Wolbach is presenting
  * Orthodox Easter is 19 April&mdash;want to schedule LSF visit after this to avoid conflict
  * Expecting Fredy and one other person&mdash;will finalize details later

<!--
### Wolbach Updates

* x
-->
