# 16 July 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Pierros Papadeas, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |
| #42 | Make plans for updating website | In progress | AW |

**All other actions were completed or are no longer relevant.**

<!--
### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates

### LSTN Updates

* Chile and Moldova received their packages&mdash;this means all of the kits have been delivered!  
  * National Library of Moldova's issues with delivery were a Moldova-specific issue, we think (their Ministry of Culture requires a donation letter...this is seperate from customs)
* Next steps: 
  * Moldova will build their kit; might later dissassemble and do a community build, should find out in August whether they can hold in-person events
  * Marathon, TX library is open for 2 people at a time.Library runs high school robotics program; will work with librarian and high school teacher to get station up and make online materials
  * Chile - wants to create online education material
  * Each group uses Facebook Live for online workshops; could also use Zoom

### MetaSat Updates

* By end of month, will have documentation materials to show...will revisit then
* Working on new release of MetaSat, with new and removed terms and crosswalks
  * Will send an email/Riot chat when we are ready for feedback
  * Will work on changing some terms into acronyms
  * Making descriptions more spacecraft-specific, rather than generic (right now a lot are from Wikidata so they are broad)
  * Hopefully will be last major DB change before asking for public feedback

### LSF Updates

* SatNOGS milestones:
  * [MetaSat July-August 2020](https://gitlab.com/librespacefoundation/satnogs/satnogs-db/-/milestones/20)
  * [Model redesign and improvement](https://gitlab.com/librespacefoundation/satnogs/satnogs-db/-/milestones/20)
  * [SatNOGS Satellite Unique Identifier](https://gitlab.com/librespacefoundation/satnogs/satnogs-db/-/milestones/12)
* UI/UX stuff; makes the data more visible and more easily discoverable, so more people can contribute
  * [UI/UX update video](https://www.dropbox.com/s/7rrrm772hczidk6/Kapture%202020-07-12%20at%2017.49.50.mp4?dl=0)
  * [UI/UX update screenshot](https://www.dropbox.com/s/on84ehnwm0rv6y8/Screenshot%202020-07-11%2009.06.25.png?dl=0)
* Browsable JSON-LD update - [see API page](https://db.satnogs.org/api/?format=api)
* Will try to add MetaSat information to [SatNOGS Wiki](https://wiki.satnogs.org/Main_Page) soon
  * Will describe MetaSat with links to GitLab and API
  * One page for how to get JSON-LD from API, and another page about MetaSat specifically

### Wolbach Updates

* Thinking of hiring UI/UX developer...would they fit better with the MetaSat or LSTN project? (leaning towards MetaSat)
* Still working with Josh Greenburg for Sloan funding...will hopefully have another proposal ready by January
* [Finished poster for SmallSat conference](https://drive.google.com/file/d/1AUgFTcPe4Zc6SAfCPrB1k9jDTLpuxQFV/view?usp=sharing)
