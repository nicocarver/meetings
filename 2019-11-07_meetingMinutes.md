# November 7, 2019 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Katie Frey, Pierros Papadeas, Nico Carver, Allie Williams, Fredy Damakalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #10 | Contact potential partner libraries | Currently in talks with Namibian Library Association and Salish Kootenai College in Montana; more updates below | NC |
| #22 | Email specific individuals for feedback | Ongoing | DC |
| #31 | Confirm workflow for changes in schema | Ongoing, Daniel and Daina seeking feedback at COSPAR | DB |
| #34 | Draft LSTN handbook | Ongoing | NC |
| #35 | Make decisions on LSTN laptop | Ongoing | DB, NC |

<!-- **All other actions were completed or are no longer relevant.**


#### Logistics  
* 


## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #36 | ---------------- | XX |

-->

## Updates

### LSTN Updates

* In talks with Namibian library association
* Changing strategies with Brazilian libraries--will be reaching out to state libraries/librarians instead of individual local libraries.
* In talks with library at [Salish Kootenai College](https://library.skc.edu/) in Montana.
    * Potential tech issues but librarians seem excited
* Cambridge Public Library--electric motor on elevator. How can we test for interference in case we put ground station there?
  * Can try antenna out (with just laptop, antenna, SDR) and see if there is interference
* Ground station parts have been ordered! Including M2 eggbeater antenna, sent to LSF for testing
* Education stuff: Nico has met with Avery about curriculum development (they're also building ground station), will meet regularly

### MetaSat Updates

* Drafts of 4 use cases are in the repository
  * All under "Staged," as we do not have a stable schema yet.
  * May want to reconsider workflow, and work on schema in different branches as opposed to different folders  
  * Example [here](https://gitlab.com/librespacefoundation/satnogs/satnogs-network/-/network/master) has multiple branches during early development that are later merged. The code is given a version number once it is stable.

### LSF Updates

* Working on reference setup: [ground station 977](https://network.satnogs.org/stations/977/)
  * Reference ground station has noise issues
  * Metal casing and grounding can help, experimenting with dedicated grounding line
  * Will have an update in a few weeks
