## August 08, 2019 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Katie Frey, Pierros Papadeas, Nico Carver, Daniel Chivvis, Allie Williams, Daina Bouquin  
**Meeting purpose**: Updates from both teams  

*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; ERD - Entity Relationship Diagram; SAA - Space Act Agreement; UHF - Ultra High Frequency [radio waves]; BOM - Bill of Materials [i.e., the LSTN kit materials])*  

#### Action Review  
* [Issue #1][ERD]: LSF sharing existing information (ERD, etc.) to help inform MetaSat fields, especially with respect to dashboard use case
    * Task moved to "To-Do," assigned to Vasilis, information will be shared within the next week.
* [Issue #3][intl]: Feedback on LSTN candidates
    * Ongoing, task moved to "Doing." LSTN candidates have been narrowed down to three preferred candidate libraries.
* [Issue #4][rural]: Input on rural US libraries (logistical issues)
    * Ongoing, task moved to "To-Do." Need to do more research on licensing in US.
* [Issue #5][kit]: LSTN kit feedback on Nico's document
    * This is the document that will be sent to potential partner libraries
    * Issue moved to "Doing," assigned to Pierros.
* [Issue #6][LSTN]: Update LSTN kit (Raspberry Pi, amplifier, etc.)
    * Issue moved to "To-Do," assigned to Nico.
* [Issue #7][licensing]: Send Wolbach info on intl licensing
    * Ongoing. Pierros has shared information on Namibia and Zambia. Moved to "Doing."
* [Issue #9][islands]: Send Wolbach info on Namibia and islands
    * Moved to "To-Do" and assigned to Pierros; task is now confidential.
* [Issue #10][shortlist]: Wolbach look into Fiji, contact international libraries, etc.
    * Wolbach plans to email candidate libraries at the end of next week. Task moved to "To-Do" and assigned to Nico.
* All other actions were completed or are no longer relevant.

#### Logistics  
* Meeting still has issues with echo. Need to find a solution. Ideas:
  * Keep using Mumble and use push-to-talk
  * Change platform: Zoom, Jitsi, Discord?
    * Would prefer an open-source platform
    * Do we want both voice and text chat?
  * Wolbach uses a headphone splitter
  * Wolbach has everyone get headphones with mic

#### Action Items  
* Double check [information on licensing][remote-US-license] for rural US ground station
* LSF: Share [BOM for LSTN][LSTN-BOM]
    * Subtask: Check potential antenna dimensions for LSTN kit
* LSF: [Review schema.space document][website-outline] within next week
* Wolbach: [Update schema.space website][website-update]


#### LSTN Updates  
* **Licensing**: Namibia and Zambia
    * Probably Namibia is top choice
    * Wolbach wants to talk to Namibian library association; they also have a radio club in same town (Windhoek)
    * Licensing: 3 options
      * 1: Added to an existing local license as a new location
      * 2: Apply for a new local license
      * 3: Apply for a foreign license with either Wolbach or LSF location
        * This needs yearly renewal
      * In any case, we will want a formal written agreement
    * Does Namibia have different levels of license?
      * Yes, but the lowest level should be fine for us
* Will need to go through all of the same licensing steps in Brazil and US, too.
    * LSF has experience in each of these countries, though.
* **Three top library choices**:
    * Windhoek Public Library, Windhoek, Namibia
    * Biblioteca da Floresta, Rio Branco, Brazil
    * Woksape Tipi, Kyle, South Dakota, US
* No issues foreseen with Brazil location
* For South Dakota: We need to look into licensing because it's remote--even if we are only receiving
    * Usually in US only need license to transmit, but remotely operated stations may have different rules
      * Will this be a remotely operated station?
* **Important for LSTN kit document**: What antenna size?
    * What would an omnidirectional S-band antenna look like?
    * What about a UHF antenna?
    * Kit may be mostly the same at every library, but with choice of antenna
    * LSTN kit document should have information on the physical size of the antenna

#### MetaSat Updates  
* SatNOGS ERD -- Pierros will look into that and send soon
    * So that we can use SatNOGS database fields to inform MetaSat schema
* Using [schema.space](https://schema.space) as our public-facing website
* Schema itself: What's the best way to organize how we can give each other feedback on it?
    * What's the best tool? Thinking of using a [dendrogram][UAT-dendrogram] similar to one Katie made for visualization <!-- Note on dendrogram link: click the dropdown menu on top left of screen and pick a branch; an interactive branched figure should appear on the right -->
    * Put in a repository--add it as a commit and people can comment on the commit itself
    * Or, put dendrogram in hidden directory on website, share .csv or JSON file on GitLab
      * That way can see as both dynamic figure and flatly

#### Wolbach Updates  
SatSearch (Kartik Kumar) reconciling comparing commercial off-the-shelf parts for CubeSats.  
We might want to look into specific identifiers he might make for commercial products--so we can cross-check with that if we want to add granular info in the schema.  
Daina and Pierros will talk further about implications of working with people with commercial interest in small satellites.  

Meeting with NASA (Bruce Yost, Craig Burkhard) to work on SAA.
NASA's systems have shortfalls and they would be interested in working with us.  
How would SAA represent LSF? Do we need to make two agreements?  
If we move forward with SAA, will we be having more meetings separate from this one? (Wolbach will update LSF when we have a clear answer on this.)

<!-- Links -->
[ERD]: https://gitlab.com/metasat/meetings/issues/1
[intl]: https://gitlab.com/metasat/meetings/issues/3
[rural]: https://gitlab.com/metasat/meetings/issues/4
[kit]: https://gitlab.com/metasat/meetings/issues/5
[LSTN]: https://gitlab.com/metasat/meetings/issues/6
[licensing]: https://gitlab.com/metasat/meetings/issues/7
[islands]: https://gitlab.com/metasat/meetings/issues/9
[shortlist]: https://gitlab.com/metasat/meetings/issues/10
[remote-US-license]: https://gitlab.com/metasat/meetings/issues/11
[LSTN-BOM]: https://gitlab.com/metasat/meetings/issues/12
[website-outline]: https://gitlab.com/metasat/meetings/issues/13
[website-update]: https://gitlab.com/metasat/meetings/issues/14
[UAT-dendrogram]: http://uat.altbibl.io/sort/
