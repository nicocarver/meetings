# September 26, 2019 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Vasilis Tsiligiannis, Fredy Damakalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; OSCW - Open Source CubeSat Workshop; AAS - American Astronomical Society)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | --- | --- | --- |
| #7 | Attempt to obtain radio license in Namibia/Zambia | Answer from Namibia, will be able to acquire a license (Pierros will update this issue) | PP |
| #10 | Contact potential partner libraries | Will revisit Namibia list and reach out to first library; Have contacted US and Brazilian libraries, but haven't heard back; May contact [Salish Kootenai College](https://cubesat.skc.edu/) in Montana, which is tracking their CubeSat (BisonSat or Nwist &#x51;&#x313;&#695;i&#x71;&#x313;&#695;&#225;y) via SatNOGS | NC |
| #19 | Test LSTN kit | [currently testing to see if it works with cheaper components] Still testing; Pierros uploaded some photos from the station? Pierros will add links in the issue | PP |
| #21 | Draft list of stakeholders to contact | (ongoing) | DC, AW |
| #22 | Email specific individuals who may want to give feedback on MetaSat schema | Planning to email the people on this list early next week; Moved to "Doing" | DC |
| #30 | Draft emails to LSF contacts; run by LSF | Will run draft by LSF within the next week; Moved to "Doing" | DC |
| #31 | Confirm workflow for changes in schema and sharing changes with Libre Space | Planning to officially announce workflow at OSCW meeting in October  | DB |
| #33 | Draft LSTN document: What libraries can do with a ground station | Work-in-progress; may make this part of a larger LSTN handbook | NC |

<!--
**All other actions were completed or are no longer relevant.**

## Action Items

| Issue number | Summary | Assignee |
| --- | --- | --- |
| #34 |  |  |
-->

## Updates

<!--
### Logistics  
*
-->

### LSTN Updates

* Question: Etiquette on scheduling observations on other people's stations
  * Everyone with a station in-network can schedule an observation on other stations, too
  * Some have hardware or bandwidth issues, so can add limitations at their own discretion
    * Owner can write in ground station description if they have limitations
    * For example, may want to prioritize certain types of observations, or close to scheduled observations at some times for testing, etc.
  * Future goal: Automated scheduling with a priority list for each station and the network
    * In testing stages right now
    * Users would be allowed to give a prioritization list for their station, and how much time it would be in this system (50% of its time, for example), and can take it offline
* Kits for libraries were underbudget, so may send every library that participates a new laptop as well, to help ensure they have a machine that can hook up with the kit

### MetaSat Updates

* Current plan: announce procedure and workflow for changes to schema at OSCW meeting in October
  * Right now the schema is going through fast iterations, so making a GitLab issue for everything will make it take longer
  * Questions to answer before announcement:
    * Who is involved in making decisions about changes?
    * What is the decision-making criteria?
    * How do we decided to accept or reject submissions?
* AAS: splinter meeting at annual meeting in Hawaii (extra meeting on small sats/CubeSats)
  * Wolbach will host a 1 hour meeting on January 6th
* Have received some feedback from [gr-satellites](https://destevez.net/2016/08/introducing-gr-satellites/) (Daniel Estévez)

### Wolbach Updates

* NASA contact: Should be meeting with him next month
  * Will get a better idea of what they can commit
* New advisor: Emily St Germain--public librarian at Cambridge Public Library--can help with outreach ideas
* Daina spoke at GRCon and some listeners expressed interest in being part of the feedback process for the schema
* We have logos! For both Metasat and LSTN (Metasat logo is on the front page of the [website](http://schema.space))

<!-- Links -->
