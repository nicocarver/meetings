# 30 July 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Katie Frey, Fredy Damkalis, Pierros Papadeas, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |
| #42 | Make plans for updating website | Update should go live tomorrow or early next week! | AW |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
--->

## Updates

### LSTN Updates

* Preparing to make online educational materials
  * Marathon, Texas librarian will build the ground station first and then work on materials
  * Library in Chișinău, Moldova would like us to provide educational materials after they build their station
  * Library in Santiago, Chile has their kit, hope to talk to them soon
  * Haven't spoken to Cambridge Public Library recently; they don't have their kit yet
* Want to start with online materials that act as primers about satellite communication
  * [Possible example](https://lstn.wolba.ch/sat-comm-activity/) - group activity about decoding satellite data using morse code - want these to be interactive!
  * Might make a brainstorming document
* Idea from SatNOGS: Follow the information path/follow the signal (audio, domodulated data, decoded data, at what point does it become human readable?), looking at SatNOGS dashboards
    * Make sure to avoid jargon
* Were contacted by a [library](https://ranchomiragelibrary.org/observatory.html) in California interested in being part of LSTN
  * Rancho Mirage Library and Observatory; they have telescopes and already do some astrology education!
  * Nice to see that we are already drawing attention
* Will talk more about LSTN activities and funding in mid-August
    

### MetaSat Updates

* Website should go live tomorrow! Or early next week
* Working on element families, adding and deleting elements
* Adding support for deprecated concepts
  * Pages for deprecated concepts will have a clear deprecated label, date deprecated, and suggested terms to use
  * In this update, will just be deleting terms; deprecation will be important once we have a stable release
* Also updating look and feel update to website, too!
  * And resources added, with anchor links throughout each page
* Added README to [MetaSat Examples](https://gitlab.com/metasat/metasat-schema/-/tree/master/Examples) folder
* Need to start creating RDF files for MetaSat terms - hopefully using a script
* We want to link to MetaSat stuff at SatNOGS wiki, where?

### LSF Updates

* [New SatNOGS DB UI](https://db-dev.satnogs.org/) moving along
  * "About" page here should eventually have a link to MetaSat, or MetaSat on SatNOGS wiki
  * Website points to wiki beccause it makes it easier for non-developers to help edit the wiki
  * Does wiki have site map?
    * Not as a diagram or graph, just as a menu (because links throughtout are not hierarchical)
    * Wolbach might look closer at the wiki, to see what pages we might want to send LSTN libraries to
  * Please look through new UI for feedback - want to see how accessible it is, for example
* Hopefully soon, satellite pages ([example](https://db-dev.satnogs.org/satellite/25544/)) will have exportable information that uses MetaSat! 
  * Still figuring out storage, mapping, versioning
  * Page has "tabs" right now; do they reflect actual hierarchy in MetaSat? (that might be the goal)
  * As a proof of concept - make example JSON-LD file for ISS
    * How much detail? What information might be required?
    * We know what information SatNOGS has on the DB now, but what additional information will they want?
    * Deep detail on transmitters, but "one or two levels deep" for other spacecraft/space segment information - we will talk about this in more detail later
  * Need to update [QUBIK](https://db-dev.satnogs.org/satellite/99000/) JSON-LD [example file](https://gitlab.com/metasat/metasat-schema/-/blob/master/Examples/QUBIK_EXAMPLE.jsonld) - when should it be updated?
    * We will start to work on JSON-LD examples in 2 weeks
* Potential contact - [Jonathan McDowell](http://www.planet4589.org/jcm/index.html) at CfA? 
  * Would like his input on new SatNOGS DB pages

<!--
### Wolbach Updates

* x-->