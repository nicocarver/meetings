# 2 April 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Vasilis Tsiligiannis, Fredy Damkalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #34 | Draft LSTN handbook | Nico has written a [rough draft](https://docs.google.com/document/d/1rVSIz5vKiA7creqySa_nkw2REx27KtMAnCbX9Ah93Rw/edit?usp=sharing) for the first section and would like feedback | NC |
| #36 | Research radio reception laws in Moldova | No update | PP |
| #40 | Feedback on spreadsheets for MetaSat terms | Still asking for feedback, particularly on the [Electrical Power Systems](https://docs.google.com/spreadsheets/d/152fk1edR0Esd1OOoNQehNO2mFvVHFGUBFw-7IHBTj3M/edit#gid=378133629) terms | LSF |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #41 |  | XX |
-->

## Updates

### LSTN Updates

* [Wolbach station](https://network.satnogs.org/stations/1378/) has been doing well since the Lime SDR updated
  * Change in radio settings may have also helped improve waterfalls
  * Might put back onto the automated vetting system soon
* LSTN handbook: We're going to be sending it to partner libraries without activities for now (just build instructions)
  * [Draft of first part](https://docs.google.com/document/d/1rVSIz5vKiA7creqySa_nkw2REx27KtMAnCbX9Ah93Rw/edit?usp=sharing)
  * Not complete, but long&mdash;if people want to review now that would be helpful
  * We're not expecting it to be complete to be sending out; right now, want to send it by end of next week, and then we'll send additional chapters and materials later

### MetaSat Updates

* Working on [electrical power systems](https://docs.google.com/spreadsheets/d/152fk1edR0Esd1OOoNQehNO2mFvVHFGUBFw-7IHBTj3M/edit#gid=378133629) terms; attitude control, thermal, and propulsion systems have also been started.
  * Will be making subsystems example files soon
  * We also have a [general hardware terms file](https://gitlab.com/metasat/metasat-schema/-/blob/master/Examples/GeneralHardwareTerms.jsonld)
  * We would appreciate any feedback you have!
* Synonyms - We have student workers at the library helping out with element descriptions and synonyms; might add them to GitLab and Riot channel

<!--
### LSF Updates

* x

### Wolbach Updates

* x
-->
