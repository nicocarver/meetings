# November 14, 2019 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Pierros Papadeas, Nico Carver, Daniel Chivvis, Allie Williams, Fredy Damakalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #10 | Contact potential partner libraries | Working on formalizing agreements in Namibia. In contact with [Fort Peck Community College](https://docs.google.com/document/d/1oHjaYO7LcsUebFsfk8CjegKJ7BXDwLrj4vbadpNe1V0/edit) in Montana. | NC |
| #22 | Send email to specific people interested on giving feedback for metasat schema | New spreadsheet Daina & Daniel have built up of people we've talked to and want to talk to--will share. | DC |
| #31 | Confirm workflow for changes in schema | Thinking about updating the sorting tool to distinguish it from the LD. Will update later | DB |
| #34 | Draft LSTN handbook | No updates | NC |
| #35 | Make decisions on LSTN laptop | No updates | DB, NC |

<!-- **All other actions were completed or are no longer relevant.**


### Logistics  
* 


## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #36 | ---------------- | XX |

-->

## Updates

<!-- ### LSTN Updates

* x-->

### MetaSat Updates

* Changing version control
  * Moving away from staged vs. stable to branching and merging--will update repository soon
* Working on crosswalking existing elements to WikiData and Schema.org
* Once new feedback system is setup, will want feedback on SatNOGS-specific elements to get ready for pilot phase.

<!--### LSF Updates

* PocketQube
  * No pressing anything that MetaSat needs to add for it. -->

### Wolbach Updates

* COSPAR updates
  * Biggest takeaway: changing sorting tool (simplifying)
  * Met with [David Klumpar](http://www.physics.montana.edu/directory/faculty/1524265/david-klumpar) of MT State--ready to help out setting up ground stations in that area.
