# 28 May 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Vasilis Tsiligiannis, Fredy Damkalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #41 |  | XX |
-->

## Updates

### LSTN Updates

* Still in FedEx limbo
* Next week will reach out to Marathon, Texas library to figure out where they're at and when they might reopen.

### MetaSat Updates

* Made element families (for organization, help for feedback)
  * How do we make this look on website? [Idea here](https://schema.space/metasat/families)
    * Working on a better URL, this link will die
* Also working on crosswalk, 400 out of 1200 terms done so far; trying to focus on popular semantic web vocabularies, like those used by Google
  * Goal is 100-200 elements a day, should be done in a week or two
  * Also working on putting crosswalk on website, figuring out best way.
* Documentation for website and other
  * Working on 4: one general primer for website, then 3 for different audiences
    * Will ask for LSF feedback next week

### LSF Updates

* Fredy is looking over elements - Through about half and giving feedback on spreadsheet, hoping to finish by next week.
* Vasilis working on client for HDF5 artifacts
  * Working on test case first to make sure artifact is valid
  * Then will work on code to pack the files
  * Hoping to finish by end of next week.
  * Early sample will be HDF5 with only waterfall
  * Will we have to update any of our instructions for the libraries about our builds?
    * Unclear right now

<!--
### Wolbach Updates

* 
-->
