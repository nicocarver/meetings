# 20 February 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Pierros Papadeas, Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Fredy Damkalis   
**Meeting purpose**: Updates from both teams, and talk about HDF5  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; SAA - Space Act Agreement)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #10 | Contact potential partner libraries | Haven't heard from library in Mauritius in a few weeks; will start looking at other areas in the southern hemisphere | NC |
| #34 | Draft LSTN handbook | [Updated LSTN timeline](https://docs.google.com/document/d/1SaboYy2P2qfXA0H_7PujthQLVIoR20VzWjF-YFeu1-g/edit) outlines some goals for the handbook; see more below | NC |
| #36 | Research radio reception laws in Moldova | No update | PP |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x
-->

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #39 | Confirm dates for Cambridge Science Festival | DB |

## Updates

### LSTN Updates

* [Updated LSTN timeline](https://docs.google.com/document/d/1SaboYy2P2qfXA0H_7PujthQLVIoR20VzWjF-YFeu1-g/edit)
  * Starting with learning objectives for libraries to implement, followed by activities based on those objectives
  * Want to develop unstructured activities, so people can drop in at their leisure
    * Thinking craft-based activities right now, so people have something to take away; also should be low-cost and work without computers
  * Feel free to give feedback and comment on the document!
* Starting with Cambridge Public Library, brainstorming a group-build for the ground station
  * Ideas for this on page two of timeline document
* Haven't heard back from Mahatma Gandhi Institute in Mauritius in a few weeks.
  * Exhausted our initial Southern Hemisphere list
  * Looking at places higher on human development index list, and places that are not countries but territories (Falkland Islands, etc)
* Why is our response rate from libraries so low?
  * Might look like a scam
  * Unclear about legal issues
  * Maybe make it an application process instead?
  * Important because we need to find a good way to scale
  * Anticipating quick growth in US, Europe, Australia
* Ready to install Wolbach ground station
  * But updated Raspberry Pi image (SatNOGS client software) isn't on GitLab (not compiled yet)
  * Will have to install current image and then upgrade
  * [Instructions](https://wiki.satnogs.org/SatNOGS_Client_Setup#Updating_SatNOGS_Client_software)
  * Bug squashing before updated compiled image is ready
  * Will there be a compiled image before April, so partner libraries can use it? Yes! 

### MetaSat Updates

* [schema.space](https://schema.space/) is moving out of wordpress to elsewhere
  * So that we can make a database of terms!
  * This will hold our URIs, will make it easy to update multiple pages at once
  * [Test domain](http://test.schema.space/)
* Should get updated JSON-LD file out soon, since we updated ground station terms
* Daniel at conference next week, and then will work on Qubik JSON-LD file

### LSF Updates

* Science fair dates?
  * Event at Wolbach is on the 26th of April, also last day of Cambridge Science Festival
  * We will be in touch to confirm dates of LSF visit

### Wolbach Updates

* Financial and substantive reports due soon
  * Can LSF help provide information for substantive report?
  * Let us know if conference stuff is going on, so we know about all outreach
* Talking to NASA about specifics of SAA again on Monday
