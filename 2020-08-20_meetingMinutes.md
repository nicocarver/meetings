# 20 August 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Allie Williams   
**Meeting purpose**: LSTN Educational ideas  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |

## Updates  

### LSTN Updates
-->

## LSTN Online Educational Ideas

### Agenda

* Summary on what has been done and timeline for works-in-progress  
* Discussion of ideas for online activities, and try to prioritize them for development. 
* What about ideas that don’t fit the LSTN model or don’t fit right now. Where should these go?  
* Approach for connecting SatNOGS documentation to LSTN community (and vice versa)  
* SatNOGS virtual mentorship program?
  * Did not make it to this. Future meeting topic?

### Meeting minutes

Activities mentioned in the minutes, as well as some additional ideas/notes, can be found in our [LSTN online activities brainstorming document](https://docs.google.com/document/d/1o7Cz3Y7vJt5PXMqKKHVPNv_UU0rPmzWBu7ADVpfSu0E/edit).

#### Summary on what has been done and works-in-progress

* Initial set of activities will be online/at home
  * Since we don't know what people will have, these activities should use minimal resources
  * Each should make sense even if library does not have station up 
    * Different libraries at different speeds; Marathon, TX has theirs up but others are waiting

#### Ideas for and prioritization of online activities

* Morse Code decoding activity should be the first
  * Sending first draft to libraries next week for feedback
  * Will make a video to explain some context for the beginning of the activities
    * Want to give some technical background before decoding: how satellites communicate; radio waves (continuous waves are used for Morse code)
  * After decoding, what the message means (name of satellite and transmitter status)
  * Nico will send out finished first draft of Morse code activity + survey; survey could include question about timing
    * Different libraries have different models; one core group doing each lesson in turn, or just all of them open to whoever
* Right now, we want to focus on 3 total online activities that go together as a "unit" 
  * Fredy talked about 2 more about decoding messages (more visual as opposed to sound-based!)
    * APT decoding (NOAA satellites, visual and IR images) and SSTV decoding (ISS messages, video of 12 images playing one after another)
    * Images are more meaningful for children
* Second possible activity: Radio waves visualized
  * Might not work online; not sure what physical resources people have (requires markers and paper)
  * Elaboration on wave info from Morse code activity
  * Relationship between frequency and wavelength
  * Will probably table this idea. Unlike decoding activities, this is a "physics" themed activity, not "communication" themed.
* Third idea from Pierros: Track the satellite through the SatNOGS process to the dashboard
  * What happens to the signal?
  * This activity might be like a "capstone," could be the longest/most involved, might require expert help (like a SatNOGS member's help)
  * Table for now
* Want to present these to partner libraries and see if this generates any new ideas or questions
* How long might these take?
  * Want these to be able to be done synchronously (Zoom, FB Live)
    * Zoom could have a 40 min limit, maybe person limit too
    * All libraries have Zoom as an option
    * SatNOGS uses Jitsy, but has issues when too many people are on; other options such as Big Blue Button
* For now, want to make a document for the "leader" of the lesson
  * Make them in a way so that libraries could take them from the website in the future
* If one lesson is community's first exposure, hopefully start with an intro to ground stations/LSTN; maybe we could make this
    * A script, or a material kit librarians can use however they want?
    * Handouts don't make sense online; maybe a few images in a slideshow, plus personal update about where their library is
  * Can make it modular, so libraries can put them together however they want
* Age range: Middle school and up
    * Rule of thumb: 20 min MAX content before hands-on material

#### Ideas that don't fit current LSTN model

* What do we do with all of these other activities that are "just a thought" right now?
  * Put online? Make clear these are drafts and we are soliciting feedback
    * Where would they live?
    * How can we share responsibly?
  * We've had people reach out to us directly already!
  * Until we have a portal via LSTN of finished ideas, do we have an online place to share ideas?
    * We have started an email chain for the partner libraries 
    * Forum? We don't have the community yet
    * Also, where does it make sense to use SatNOGS resources--they have a forum!
    * What kinds of mediums would librarians know how to USE?
      * Google doc you can add comments to might be best--Easy and approachable to use
  * Consensus: that putting the ideas online could be a good idea; make it clear that it is for BRAINSTORMING while finished ideas go on the website
    * Might also use a Google Form for contributions. Link to this on website AND Google doc
    * Might make a public review criteria, and specific themes/topics that we are soliciting ideas for
    * Specific for online activities for now
      * Should we consider this process for in-person activities too? 
        * Nico likes this idea; could help with money asks for grants to materials!
* Received a big doc of in-person education ideas
  * Themes: CubeSats, communication, antenna, physics, CubeSat simulator

#### Approach for connecting SatNOGS documentation to LSTN community (and vice versa) 

* Connecting educational materials and SatNOGS documentation
  * Building bridges between the public and contributing to space exploration
  * SatNOGS can find some easy operation or development tasks to get public involved (like single beginner's contributions, stuff for SatNOGS newbies)
    * Need better documentation for doing these tasks
    * Citizen science ideas: Vetting waterfalls, raising issues

#### Additional topics

* Other resources we can give libraries? Small collections of books, for example

<!--
### MetaSat Updates

* x

### LSF Updates

* Continuing client work and DB satellite ID stuff

### Wolbach Updates

* x
-->
