# 15 October 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Pierros Papadeas, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |
<!--
**All other actions were completed or are no longer relevant.**
-->

### Logistics  

* On the 29th, Europe will be out of DST and US will still be in it. Wolbach will meet at the same time, and LSF will shift their time by an hour that week

<!--
## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates  

### LSTN Updates

* Something that we will need to resolve: Our original plan was that people in public library communities can use library station to schedule observations at the library using the library's credentials
    * However, now many libraries are closed; how could community users use the stations right now?
    * SatNOGS station [permission matrix](https://wiki.satnogs.org/Operation#Network_Permissions_Matrix) does not allow non-users to schedule or vet observations
    * Potential fixes? Right now, teams sharing a station might share an account - not great!
      * Instead, could set up "groups" so a group is an owner, instead of an account
      * There are other use cases that this would solve! Missions and clubs working together, for example
      * Group member "inherits" permissions of the owner? Might want to make different tiers - who can schedule, who can vet, etc
      * However, there are other issues to solve first
        * For example, scheduling scripts. If many people schedule observations on the same ground station, how does it choose which to do in what order?
        * Also demodulation and vetting upgrades pending, and some experiments going on
          * Don't want to rely on manual scheduling and vetting, but can you continue to do it anyway? Yes!
        * What is the policy of expanding the network? Accounts are connected to ownership. How does making groups affect ownership?
          * Worried about abuses
* Good news: [Marathon, TX ground station](https://network.satnogs.org/stations/1730/) is online!
  * Hoping to get their users started with it soon
  * They might have some interference - power supply grounding issue?

### MetaSat Updates

* Updating the [GitLab repo](https://gitlab.com/metasat/metasat-schema)
  * Added a few files: Updated metasat.jsonld, and adding CSV files for all of the elements and crosswalks

### LSF Updates

* Still working on satellite ID and other technical stuff
  * Taking longer than expected bc there are a lot of edge cases
  * Can people generate their own unique IDs? How would you avoid collisions?

<!--
### Wolbach Updates

* x
-->