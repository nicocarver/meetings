# 21 January 2021 MetaSat Meeting Minutes

**Time:** 15:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Pierros Papadeas, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

<!--
**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->
## Updates  

* Anticipating feedback from Sloan about our proposal draft soon
* Daina and Pierros will be meeting in a few weeks to talk about LSTN proposals
  * Might ask another public library to be part of our funding application? 
* Daniel working on contributor guidelines for MetaSat&mdash;should be sharing a document with those later today

<!--
### LSTN Updates

* x

### MetaSat Updates

* x

### LSF Updates

* x

### Wolbach Updates

* x
-->