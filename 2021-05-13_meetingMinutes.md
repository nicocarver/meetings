# 13 May 2021 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Fredy Damkalis, Katie Frey, Pierros Papadeas, Nikoletta Triantafyllopoulou, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->
## Updates  


### LSTN Updates

* Next step with LSTN: come up with a strategy to choose and reach out to  libraries for the second LSTN cohort.
  * If we get NASA funding, will have to be domestic (US) libraries
  * Want to define both WHO and HOW to reach out
  * If anyone has ideas or experience, let us know!
    * For example, how do you get people to buy in to SatNOGS when they don't have experience?
* Talking with Chilean library; having technical difficulties (network issues)
* Final [draft of blog post](https://docs.google.com/document/d/1CZw140I7-7Ghfe_1kLxEOttD6LS3-i-gKFGGns4QihE/edit) ready
  * Update: Daina added an extra image that will be helpful 
  * Suggestion: Elkos suggested sharing post and Weblate tutorial with SatNOGS core community before posting blog
    * Pro, get feedback; con, will delay posting date
  * Also started drafting social media posts; ready to share next week
    * Will need more images for this
  * Once blog post is live, will also probably re-post to Wolbach blog (Galactic Gazette)

<!--### MetaSat Updates 

* x
-->
### LSF Updates

* Progress with light end point
  * Hoping to put code on dev instance early next week
* Want to add issues to MetaSat about crosswalks and some terms
  * Still talking about possibly changing some terms such as "downlink" and "frequency" to "downlinkFrequency"

### Wolbach Updates

* NASA ROSES proposal should be going in today!
  * When will we hear back? Not clear
    * Review is over the summer, and might find out in early fall
    * If successful, money is distributed in January
  * Daina will be sharing link
