# 14 May 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Vasilis Tsiligiannis, Fredy Damkalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #41 |  | XX |
-->

## Updates

### LSTN Updates

* Had some issues with LimeSDR with the [Wolbach Station](https://network.satnogs.org/stations/1378/) again, will be turning on advanced error logging in case it happens again 
* Still waiting on FedEx to send out kits

### MetaSat Updates

* 1275 elements in our database - Comms, equipment, and orbital elements were added; next will be maybe mission elements
  * Feedback if you can
  * Next - LD files with the elements
  * Crosswalk coming up; mostly done for Wikidata, will do other standards/schemas too
  * Also want to split things by category, to make them easier to find    
    * Will make it easier to get feedback - can ask for feedback for one category instead of the whole thing
  * Maybe come up with another way to sort elements on the website, too; thinking about user-friendliness
  * Sorting tool will be reimplemented soon
* Updated "Term-sets" folder and README: https://gitlab.com/metasat/metasat-schema#about-this-repository
* Might expand documentation after release; not sure what this looks like yet
  
<!--
### LSF Updates

* x

### Wolbach Updates

* x
-->
