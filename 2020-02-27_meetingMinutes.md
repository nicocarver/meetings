# 27 February 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Nico Carver, Allie Williams, Vasilis Tsiligiannis, Fredy Damkalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #10 | Contact potential partner libraries | Heard back from another library in Chile; tentatively moving this issue to "Done" | NC |
| #34 | Draft LSTN handbook | Ongoing | NC |
| #36 | Research radio reception laws in Moldova | No update | PP |
| #39 | Confirm dates for Cambridge Science Festival | Possible April 23-25; will finalize soon | DB |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #40 |  | XX |
-->

## Updates

### LSTN Updates

* Heard from Biblioteca de Santiago in Chile (potential Southern hemisphere library)
  * They have a technology librarian who has done astronomy and astrophysics projects
  * Proposal in talks with the library director now
  * Looks like the proposal was approved!
* Wolbach station is pretty close to being installed. 
  * Nico is working on updating the client software
  * How to set up software with Lime SDR? Right now, Wiki says it's an "advanced" option, but instructions are unclear
    * Wiki will be updated today or tomorrow, so keep a look out for it
    * Lime SDR being moved from "advanced" to "basic," part of the minimum necessary setup
    * Sampling rig also mandatory now


### MetaSat Updates

* Working on ironing out ground segment
  * Mostly working to update hardware description
  * Can follow along [here](https://gitlab.com/metasat/metasat-schema/issues/30)
* [test.schema.space](http://test.schema.space/) has [URIs and Crosswalks](http://test.schema.space/metasat/epoch)
  * Will update to make RDF/XML pages too
* Daniel working on example file for Qubik

### LSF Updates

* Want to use MetaSat schema to fully describe transmitters help develop decoders 
  * Will help make the decoders more modular, for example
  * Will need to crosscheck with [Codemeta](https://codemeta.github.io/terms/) terms&mdash;what will this description need that isn't captured in Codmeta?
  * If you could fully capture the details of the decoder, could store in SatNOGS DB and that will help with capturing the best information from satellites
  * Want to capture different parameters, for example, so entire decoder doesn't have to be copied and edited
  * Capturing not just software, but its functionality
    * Codemeta might not be granular enough
    * Is there anything written up about this? (not yet)
  * Try FM demodulator as an example/proof-of-concept
  * Then more complicated demodulators, then decoders (much more complicated)
  * "Physical" decoder (C++) vs. bitstream decoder
    * Here, talking about the C++ decoder

### Wolbach Updates

* Cambridge Public Library event: At least two LSF people at the build in April? And maybe give a talk
  * Date options: 23rd, 24th, or 25th of April
  * Fly LSF people in on 23rd, talk on 24th, event on 25th?
* Next year's OSCW&mdash;at Harvard? Daina booked some rooms but need more information
  * Will talk to Pierros
