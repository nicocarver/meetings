# December 19, 2019 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Pierros Papadeas, Nico Carver, Daniel Chivvis, Allie Williams, Vasilis Tsiligiannis, Fredy Damkalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; AAS - American Astronomical Society, which has an annual meeting coming up in January; HDF5 - Hierarchical Data Format 5, a file format used to both store and organize data)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #10 | Contact potential partner libraries | Updates below, still looking for libraries in US | NC |
| #22 | Email specific individuals for feedback | Keep open until AAS.  | DC |
| #34 | Draft LSTN handbook | --- | NC |
| #35 | Make decisions on LSTN laptop | Might go with MacBook, Nico wrote up his thoughts | DB, NC |
| [MetaSat Schema 4](https://gitlab.com/metasat/metasat-schema/issues/4) | LSF: give feedback to Daniel on new schema | Daniel would like feedback before AAS meeting, January 4, 2020 | -- |

**All other actions were completed or are no longer relevant.**

### Logistics  
* Next meeting?
  * Will skip December 26 and meet on January 2, 2020
  * Wolbach staff will mostly not be in on December 26, but LSF people will be available to talk


## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #36 | Research radio reception laws in Moldova | PP |
| #37 | Feedback from LSF on current version of schema | PP, VT, FD |

## Updates

### LSTN Updates

* Moldovan library is officially our first international partner
  * [This library](http://www.hasdeu.md/) in the capital of Moldova
  * Concerns: Getting the parts (customs)--may send ground station materials through US Embassy
  * Will they need a permit to install or operate? We will look up
    * For radio reception in Moldova, should be fine
  * Could run into similar issues with other international libraries, as well
* Namibia: working through administrative red tape
  * Eventually need Attorney General of Namibia to sign off
  * MOU (Memorandum of Understanding) between us and Namibia (might be similar with any other governmental international entities)
  * Positive: Will have a template for other similar (federal) libraries
  * Could be 4 months to formal official signoff (but they're willing to go through this process)
  * We ARE looking for other locations in the meantime (might end up with an extra location)
* Looking for libraries that are not connected to ministry of culture (ie, unlike Namibia), and it's difficult, most are under a federal system
  * So it looks like Namibia situation is valuable because we might run into similar issues elsewhere
  * Looking at Mauritius and [Antarctica](http://www.theantarcticbookofcookingandcleaning.com/antarctica-reads-scott-base-library-antarctica.html)
    * There are 4 public libraries in Antarctica (one at each base)
    * Potential issues: What might freeze on ground station? What bandwidth could they use?
* Rural US--still no partner, reached out to Marathon, TX library 
* Laptop--leaning towards Macbooks for LSTN kit
  * Because libraries might be more likely to already have a Macbook than a Linux machine
  * But what about Windows? Might be more likely to have Windows than OSX
  * Nico's thoughts on the matter are [here](https://docs.google.com/document/d/1i-RIHlvWRnYEUUr7XL6fdg7Hxmbv3fjQFzYXEvJRIvk/edit)
    * Wouldn't have to install anything or configure laptop when using Mac or Linux
  * Will any laptop affect about shipping costs for the kit?

### MetaSat Updates

* Fredy will leave comments on [this document](https://docs.google.com/document/d/1Y3k76q2sRrjsrxJCjIVdIpzlpc-kD1dvFs8KNO_H1tE/edit?ts=5df792dd), mostly additions and clarity
* Specific feedback Daniel wants from LSF: 
  * Payload telemetry and demodulated data (where to put?)
    * Mission-specific, so do not nest under observation, but mission and link to observation. These correspond to specific time points.
    * Mission artifacts in general--where will they fit? Right now they are linked to spacecraft, not mission
      * Different artifacts should exist in different places--waterfall is specific to a ground station's observation, but demodulated data is not
      * For example, what if 5 ground stations are all getting data from the same satellite at once? All of the waterfalls would be different, but the bitstreams would overlap.
  * TLE--confirm best place to put it (might be high level--directly under Spacecraft)
  * Ground Station section overall
    * Current schema lifted from SatNOGS and some parts have more detail than others because of it--should we change granularity in those spots?
* Also, how do we fit HDF5 in schema?
  * Each HDF5 file should be connected to an observation
  * Will be searchable
    * Will SatNOGS continue to accept non-HDF5 files after implementing it? 
      * Yes--but they will be seperated in their databases--to keep their data clean--for machine learning
      * Users can potentially create HDF5 files with HDFView online, or more powerful programming and command line functions. Will depend on how they collect their data.

### LSF Updates

* Pocket Qubes are officially going to launch! Finishing sats in February for possible launch in March
* HDF5 file storage--any ideas? Is Zenodo an option?
  * Internet Archive, arXiv
  * Will talk later

### Wolbach Updates

* Funding update--Daina will be following up with Sloan soon about changes to our initial plan
