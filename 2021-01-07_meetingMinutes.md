# 7 January 2021 MetaSat Meeting Minutes

**Time:** 15:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Pierros Papadeas, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

<!--
**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates  

### LSTN Updates

* Talking with Juan Luis about translating LSTN stuff to Spanish - trying to find libre software to help - ideas?
  * https://weblate.org/en/
  * Starting with Spanish but want to expand to other languages (for example, right now need translation to Romanian, too)

### MetaSat Updates

* Crosswalking to [CEOS database](http://database.eohandbook.com/)
* Working on paper
* [Citation JSON-LD file](https://gitlab.com/dbouquin/metasat-toolkit/-/blob/ca296fff1bf28f9ffbe3511c5a453f1ddf0b21f7/citation.jsonld) from Daina
  * Waiting for some edits and then will be added to main repo
* Big repo change: Renamed to "MetaSat Toolkit"
  * Need to update your local repository if necessary
  * New URL: https://gitlab.com/metasat/metasat-toolkit
  * All URLs should be updated on repo, and website links will be updated soon
  * Need to update title of README file!
* Website updates
  * Not live yet, but can see changes on https://test.schema.space/
  * Worked on layout of resources page
  * Fixed page anchors so they jump to the right part of the page
  * Updated some text on what we will have for first part of release
  * Updated links to GitLab
  * About page: Remove mention of LSTN

<!--
### LSF Updates

* x
-->

### Wolbach Updates

* Next week is AAS meeting for Wolbach people
* Week after: plan for next steps and funding ideas
* Will meet third week of January to talk about versioning and other next steps
* Hoping to hear back from the ARDC in the next few weeks 
* We are getting letters of support from more groups! Will hopefully help with future grants
