# October 24, 2019 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Pierros Papadeas, Nico Carver, Daniel Chivvis, Allie Williams, Fredy Damakalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; SSA - Space Situational Awareness; PLIX - Public Library Innovation Exchange)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #10 | Contact potential partner libraries | Haven't heard back from Brazil, did hear back from Namibia (will get back to us in early November) | NC |
| #22 | Email specific individuals for feedback | Have a shortlist of people from OSCW, people recommended by LSF, plus a few more | DC |
| #31 | Confirm workflow for changes in schema | Want to revise this, because crosswalk from schema tool to schema is not going to be clean or easy--need a new solution | DB |

**All other actions were completed or are no longer relevant.**

<!--
#### Logistics  
* Screensharing tool? For next week
-->

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #34 | LSTN handbook work (with MIT?) | NC |
| #35 | Make decisions on LSTN laptop (MacBook Air vs ThinkPad) | DB, NC |

## Updates

### LSTN Updates

* MIT may fund ground station on MIT and PLIX libraries!
  * MIT wants to help with education/curriculum side
* LSF has a collaborator at Valparaiso University (Indiana)
  * In second half of 2020, might work full-time on education with SatNOGS for a bit
* Got the all clear to install a ground station at the CfA!
  * Alternative antennae are on the LSTN kit document
  * Ordering parts soon, will email about it
* Also want to work on purchasing a laptop for each community
  * MacBook Air with a dock with SD reader? (bc Mac has Terminal with Unix commands, etc)
  * Might prefer ThinkPad (even refurbished) running Linux?
  * Issue #35, will chat more later

### MetaSat Updates

* MetaSat-related sessions at OSCW wents well!  
  * A lot of conversations on MetaSat overall, which is great  
* Will be following up with some groups to ask for feedback soon
  * Want to integrate practical feedback into overall MetaSat schema design  
  * Want to ask for feedback on just specific aspects of the schema (e.g., orbital parameters), not the entire schema
  * What are we over-confident about? We may want to ask for feedback there, specifically. (ground stations, SSA)
* Working on making LD--will do screenshare next week to demonstrate

### Wolbach Updates

* Daniel and Daina going to COSPAR meeting in Israel next month
  * Will keep an eye out for SSA information
  * Might want to connect with some contacts, too
* For future--we have funding to do stuff at meetings (splinter groups, tables, etc)
