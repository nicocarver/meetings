# 18 June 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Fredy Damkalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |
| #41 | Add MetaSat primer to schema repository | No update | AW |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x
-->

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #42 | Make plans for updating website | AW, DB, KF |

## Updates

### LSTN Updates

* Shipped final FedEx packages on Monday! Should arrive at partner libraries next week
* Moldovan library is the only partner that is open, and Chile has a specific timeline (not reopening until October)
  * What does that do to our timeline? Can we make virtual activities before ground station is built? Primers (radio frequencies, how ground stations work...as an online module)
  * Let us know if you have ideas for online activities; probably can't do community builds until the fall
    * Some chance we can't do community build; if not, will try to get ground stations installed before the winter

### MetaSat Updates

* First draft of the MetaSat [crosswalk](https://docs.google.com/spreadsheets/d/1c7hV0HqhLrc301AeidHq_GpaG1xYCbp6fpGQYeZay2Q/edit#gid=0) is done!
  * Scroll to the right on this spreadsheet
    * Might move this to its own dedicated spreadsheet
  * Crosswalks to Wikidata, [UAT](http://astrothesaurus.org/) (Unified Astronomy Thesaurus), and Schema.org
  * Want to also crosswalk to more niche vocabularies
  * Meeting on Monday to plan for website integration 
  * How can we share this on GitLab repo?
    * Add crosswalk folder, upload CSV file and README with link to a stable Google spreadsheet?
* Also working on new sorting tool
  * Hopefully done next week
* [About page](https://schema.space/about) of website is updated
  * Want to update to add the rest of the [MetaSat explainer](https://docs.google.com/document/d/1PILm-QZg63PMkYWuhuHgaUBSUDf_xeXXmwN4E3zxYzA/edit) - How should we organize our files/webpages?
    * Seperate meeting w Daina, Katie, and Allie - where should we focus our changes?
  * For now, link to Google doc primers in the "Using MetaSat" section - maybe a "Getting started" button
    * Maybe also mention crosswalk (as well as link out to another page with more detail)
  * Also probably updating homepage soon
    * More inviting and orienting

### LSF Updates

* Fredy will be main contact with Wolbach moving forward
* Continuing to update DB to work with MetaSat
* Vasilis has added new things to client and squashed some bugs

<!--
### Wolbach Updates

* x
-->
